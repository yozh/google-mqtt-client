package google_iot_mqtt_client

import (
	"crypto/tls"
	"crypto/x509"
	"errors"
	"github.com/dgrijalva/jwt-go"
	"github.com/eclipse/paho.mqtt.golang"
	"io/ioutil"
	"log"
	"time"
)

const GoogleMqttBrokerUrl = "ssl://mqtt.googleapis.com:8883"

type GoogleMqttClient struct {
	Client      mqtt.Client
	subscribers []Subscriber
}

type Subscriber struct {
	Topic    string
	Qos      byte
	Callback mqtt.MessageHandler
}

func (gmc *GoogleMqttClient) createJwt(projectId string, privateKeyPath string) (string, error) {
	bytes, e := ioutil.ReadFile(privateKeyPath)
	if e != nil {
		return "", errors.New("Cannot read private key!")
	}

	key, e := jwt.ParseRSAPrivateKeyFromPEM(bytes)

	return jwt.NewWithClaims(jwt.SigningMethodRS256, jwt.StandardClaims{
		Audience:  projectId,
		IssuedAt:  time.Now().Unix(),
		ExpiresAt: time.Now().Add(time.Hour).Unix(),
	}).SignedString(key)
}

func (gmc *GoogleMqttClient) newTlsConfig(googleCertsPath string) *tls.Config {
	certpool := x509.NewCertPool()
	pemCerts, err := ioutil.ReadFile(googleCertsPath)
	if err == nil {
		certpool.AppendCertsFromPEM(pemCerts)
	}

	return &tls.Config{
		RootCAs:            certpool,
		ClientAuth:         tls.NoClientCert,
		ClientCAs:          nil,
		InsecureSkipVerify: true,
		Certificates:       []tls.Certificate{},
		MinVersion:         tls.VersionTLS12,
	}
}

func (gmc *GoogleMqttClient) Connect(projectId string, devicePath string, privateKeyPath string, googleCertsPath string) (e error) {
	jwtToken, e := gmc.createJwt(projectId, privateKeyPath)
	if e != nil {
		return e
	}

	opts := mqtt.NewClientOptions().AddBroker(GoogleMqttBrokerUrl).SetClientID(devicePath)
	opts.SetKeepAlive(60 * time.Second)
	opts.SetUsername("unused")
	opts.SetPassword(jwtToken)
	opts.SetTLSConfig(gmc.newTlsConfig(googleCertsPath))
	opts.SetWriteTimeout(time.Second * 10)
	opts.SetConnectTimeout(time.Second * 10)
	opts.SetConnectionLostHandler(func(client mqtt.Client, e error) {
		log.Println("Connection lost due to ", e)
		_ = gmc.Connect(projectId, devicePath, privateKeyPath, googleCertsPath)
	})

	gmc.Client = mqtt.NewClient(opts)
	log.Println("Establishing MQTT connection")
	if token := gmc.Client.Connect(); !token.WaitTimeout(time.Second*5) && token.Error() != nil {
		log.Println("Coudn't establish MQTT connection")
		return token.Error()
	}

	for _, subscriber := range gmc.subscribers {
		gmc.Client.Subscribe(subscriber.Topic, subscriber.Qos, subscriber.Callback)
	}

	return nil
}

func (gmc *GoogleMqttClient) Subscribe(topic string, qos byte, callback mqtt.MessageHandler) {
	if gmc.Client != nil {
		gmc.Client.Subscribe(topic, qos, callback)
	}
	gmc.subscribers = append(gmc.subscribers, Subscriber{
		Topic:    topic,
		Qos:      qos,
		Callback: callback,
	})
}
